## Available Scripts
In the project directory, you can run:

### `npm start`
Runs the app in the development mode.<br>
The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`
Launches the test runner in the interactive watch mode.<br>

### `npm run build`
Builds the app for production to the `build` folder.<br>

### Test URL
Frontend: https://test-ammo.firebaseapp.com
Backend: https://polar-refuge-61217.herokuapp.com/products