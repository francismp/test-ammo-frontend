import axios from 'axios';

class ProductService {

  private baseUrl = "http://localhost:3000";

  public searchProductsPaginated(term: string, limit = 10, offset = 0) {
    return axios.get(`${this.baseUrl}/products`, { params: { term, limit, offset } })
  }
}

export default new ProductService()