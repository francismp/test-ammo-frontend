export interface PaginationState {
  limit: number;
  offset: number;
  total?: number;
  index?: number;
}

export interface Image {
  name: string;
  path: string;
}

export interface Product {
  sku: string;
  name: string;
  old_price: number;
  price: number;
  category: any;
  description: string;
  images: Array<Image>;
}

export interface SearchState {
  term: string;
  products?: {
    count: number;
    rows: Array<Product>;
  };
  pagination?: PaginationState;
  loading?: boolean,
  error?: any
}
