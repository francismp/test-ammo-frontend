import * as constants from '../constants/index'
import ProductService from '../../services/ProductService';

export interface AppLoaded {
  type: constants.APP_LOAD;
  products: any;
  term: string;
  pagination: any;
}

export interface ProductsLoaded {
  type: constants.PRODUCTS_LOADED;
  products: any
}

export interface UpdateSearchTerm {
  type: constants.UPDATE_SEARCH_TERM;
  term: string;
}

export interface ChangePage {
  type: constants.CHANGE_PAGE;
  index: number;
}

export interface ChangeLimitQuantity {
  type: constants.CHANGE_LIMIT_QUANTITY;
  limit: number;
}

export interface FetchProductsSuccess {
  type: constants.FETCH_PRODUCTS_SUCCESS;
}

export interface FetchProductsFailure {
  type: constants.FETCH_PRODUCTS_FAILURE;
  error: any
}

export function appLoaded(term:string, products: any, pagination: any): AppLoaded {
  return {
    type: constants.APP_LOAD,
    term,
    products,
    pagination
  }
}

export function productsLoaded(products: any): ProductsLoaded {
  return {
    type: constants.PRODUCTS_LOADED,
    products
  }
}

export function updateTermAndFetchProducts(term: string, limit: number, offset: number) {
  return (dispatch: any) => {
    dispatch(updateSearchTerm(term));
    dispatch(changeLimitQuantity(limit));
    dispatch(fetchProductsBegin(term, limit, offset));
  }
}

export function updateSearchTerm(term: string): UpdateSearchTerm {
  return {
    type: constants.UPDATE_SEARCH_TERM,
    term
  }
}

export function changePage(index: number): ChangePage {
  return {
    type: constants.CHANGE_PAGE,
    index
  }
}

export function changeLimitQuantity(limit: number): ChangeLimitQuantity {
  return {
    type: constants.CHANGE_LIMIT_QUANTITY,
    limit
  }
}

export function fetchProductsBegin(term: string, limit: number, offset: number) {
  return (dispatch: any) => {
    return ProductService.searchProductsPaginated(term, limit, offset)
      .then((result: any) => {
        dispatch(fetchProductsSuccess())
        dispatch(productsLoaded(result.data))
      }).catch((err: any) => dispatch(fetchProductsFailure(err)))
  }
}

export function fetchProductsSuccess(): FetchProductsSuccess {
  return {
    type: constants.FETCH_PRODUCTS_SUCCESS
  }
}

export function fetchProductsFailure(err: any): FetchProductsFailure {
  return {
    type: constants.FETCH_PRODUCTS_FAILURE,
    error: err
  }
}

export type SearchAction = AppLoaded
  | ProductsLoaded
  | UpdateSearchTerm
  | ChangePage
  | ChangeLimitQuantity;
