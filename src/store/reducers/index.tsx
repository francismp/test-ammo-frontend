import { combineReducers } from 'redux';

import { SearchState, PaginationState } from '../types';
import {
  APP_LOAD,
  CHANGE_PAGE,
  PRODUCTS_LOADED,
  UPDATE_SEARCH_TERM,
  CHANGE_LIMIT_QUANTITY,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE
} from '../constants';

const searchInitialState: SearchState = {
  term: '',
  products: {
    count: 0,
    rows: []
  },
  loading: false
}

export function search(state: SearchState = searchInitialState, action: any): SearchState {
  switch (action.type) {
    case UPDATE_SEARCH_TERM:
      return { ...state, term: action.term };

    case PRODUCTS_LOADED:
      return { ...state, products: action.products };

    case APP_LOAD:
      return {
        ...state,
        products: action.products,
        term: action.term,
        pagination: action.pagination
      };

      case FETCH_PRODUCTS_SUCCESS:
        return { ...state, loading: false };

      case FETCH_PRODUCTS_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.error
        };
  }

  return state;
}

const paginationInitialState: PaginationState = {
  limit: 10,
  offset: 0
}

export function pagination(state: PaginationState = paginationInitialState, action: any): PaginationState {
  switch (action.type) {
    case CHANGE_PAGE:
      return {
        ...state,
        index: action.index
      };

    case CHANGE_LIMIT_QUANTITY:
      return {
        ...state,
        limit: action.limit
      };
  }

  return state;
}

export const Reducers = combineReducers({
  pagination,
  search
});