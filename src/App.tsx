import * as React from 'react';

import { updateTermAndFetchProducts } from './store/actions'

import Header from './components/header/Header';
import ProductContainer from './components/product-container/ProductContainer';

import './App.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

interface Props {
  updateTermAndFetchProducts: Function;
  term: string;
  limit: number;
  offset: number;
}

class App extends React.Component<Props, object> {
  
  componentDidMount() {
    const {
      updateTermAndFetchProducts,
      term,
      limit,
      offset
    } = this.props;

    updateTermAndFetchProducts(term, limit, offset);
  }

  public render() {
    return (
      <div className="App">
        <Header/>
        <ProductContainer/>
      </div>
    );
  }
}

const mapStateToProps = (store: any) => ({
  term: store.search.term,
  limit: store.pagination.limit,
  offset: store.pagination.offset
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators({ updateTermAndFetchProducts }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
