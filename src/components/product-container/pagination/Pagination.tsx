import * as React from 'react';

import './Pagination.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { updateTermAndFetchProducts } from '../../../store/actions'

export interface Props {
  term: any;
  products: any;
  pagination: any;
  updateTermAndFetchProducts: any;
}

class Pagination extends React.Component<Props, object> {

  state = {
    pages: [0,1,2]
  }

  term = '';
  total = 0;
  limit = 10;
  offset = 0;
  index = 1;

  get totalPages() {
    const { limit } = this.props.pagination;
    const { products } = this.props

    return Math.ceil(products.count/limit)
  }

  get pages() {
    return this.state.pages
      .filter(i => (i >= 0 && i < this.totalPages) )
      .map((_: number, i: number) => {
        return <li key={_} onClick={() => this.selectPage(_)}>
          <a>{_+1}</a>
        </li>
      }
    )
  }

  selectPage(page: number): void {
    if (page >= 0 && page < this.totalPages) {
      const { limit } = this.props.pagination;
      const { term } = this.props;

      this.index = page
      this.offset = limit * page

      this.setState({
        pages: [ this.index-1, this.index, this.index+1]
      });

      this.props.updateTermAndFetchProducts(term, limit, this.offset);
    }
  }

  render() {
    const { limit, offset, index } = this.props.pagination;
    const { term } = this.props;
    const { products } = this.props

    this.term = term;
    this.limit = limit;
    this.offset = offset;
    this.total = products.total;
    this.index = index || 1;

    return <ul className="pagination">
      <li onClick={() => this.selectPage(0)}>
        <a>&laquo;</a>
      </li>
      {this.pages}
      <li onClick={() => this.selectPage(this.totalPages-1)}>
        <a>&raquo;</a>
      </li>
    </ul>
  }
}

const mapStateToProps = (store: any) => ({
  term: store.search.term,
  products: store.search.products,
  pagination: store.pagination
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators({ updateTermAndFetchProducts }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Pagination);