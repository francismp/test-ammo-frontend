import * as React from 'react';

import Product from './product/Product'

import './ProductList.css';

interface Props {
  products: any
}

export default class ProductList extends React.Component<Props, object> {
  render() {
    const { products } = this.props;

    let productsList = products.rows.map( (row: any) => {
      return <div key={row.id} className="product-list__item">
      <Product product={row}/>
    </div>
    })

    return <div className='product-list'>
      {productsList}
    </div>
  }
}
