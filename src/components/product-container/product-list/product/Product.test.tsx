import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Product from './Product';

const mock = {
  "id": 141,
  "sku": "XK709908009013010084",
  "name": "Fantastic Cotton Ball",
  "price": "166",
  "old_price": "482",
  "description": "Non dolorem occaecati vitae saepe.",
  "created_at": "2018-11-15T18:30:45.000Z",
  "updated_at": "2018-11-15T18:30:45.000Z",
  "categories": [
      {
          "id": 141,
          "name": "Metal",
          "description": "Facere quasi vitae perferendis omnis et ipsam molestias qui.",
          "created_at": "2018-11-15T18:30:45.000Z",
          "updated_at": "2018-11-15T18:30:45.000Z",
          "ProductCategory": {
              "category_id": 141,
              "product_id": 141
          }
      }
  ],
  "images": [
      {
          "id": 561,
          "name": "chicken.less",
          "path": "http://lorempixel.com/600/400",
          "created_at": "2018-11-15T18:30:45.000Z",
          "updated_at": "2018-11-15T18:30:45.000Z",
          "ProductImage": {
              "image_id": 561,
              "product_id": 141
          }
      },
      {
          "id": 562,
          "name": "auto_loan_account_data.h261",
          "path": "http://lorempixel.com/600/400",
          "created_at": "2018-11-15T18:30:46.000Z",
          "updated_at": "2018-11-15T18:30:46.000Z",
          "ProductImage": {
              "image_id": 562,
              "product_id": 141
          }
      },
      {
          "id": 563,
          "name": "metrics_vermont_navigating.jng",
          "path": "http://lorempixel.com/600/400",
          "created_at": "2018-11-15T18:30:46.000Z",
          "updated_at": "2018-11-15T18:30:46.000Z",
          "ProductImage": {
              "image_id": 563,
              "product_id": 141
          }
      },
      {
          "id": 564,
          "name": "jewelery.plb",
          "path": "http://lorempixel.com/600/400",
          "created_at": "2018-11-15T18:30:46.000Z",
          "updated_at": "2018-11-15T18:30:46.000Z",
          "ProductImage": {
              "image_id": 564,
              "product_id": 141
          }
      }
  ]
}

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Product product={mock}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
