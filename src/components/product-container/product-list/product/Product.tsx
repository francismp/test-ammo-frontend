import * as React from 'react';

import './Product.css';

export interface Props {
  product: any
}
export default class Product extends React.Component<Props, object> { 

  getImages(images: any) {
    return images.map((image: any) => <img key={image.id} src={image.path} alt={image.name}/>);
  }

  getCategory(categories: any) {
    return categories.reduce((p: any, c: any) => p ? `${p.name} • ${c.name}` : c.name, '' );
  }

  render() {
    const { product } = this.props

    const images = this.getImages(product.images)
    const category = this.getCategory(product.categories)

    return <div className='product'>
      <div className='product__content'>
        <div className="product__images">
          {images}
        </div>
  
        <div className="product__info info">
          <div className="info__title">{product.name}</div>
          <div className="info__description">{category}</div>
        </div>
      </div>
  
      <div className="product__price price">
        <p>
          <span className='price__old'>R$ {product.old_price}</span> por <b>R$ {product.price}</b>
        </p>
      </div>
    </div>
  }
}