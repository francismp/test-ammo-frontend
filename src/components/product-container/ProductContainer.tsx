import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


import { updateTermAndFetchProducts } from '../../store/actions'

import ProductList from './product-list/ProductList'
import Pagination from './pagination/Pagination';

import './ProductContainer.css';

export interface Props {
  term: string;
  products: any;
  pagination: any;
  updateTermAndFetchProducts: any;
}

class ProductContainer extends React.Component<Props, object> {
  
  state = {
    value: 10
  }

  change(event: any) {
    const {
      term,
      pagination
    } = this.props;

    const limit = parseInt(event.target.value)

    this.setState({
      value: limit
    });
    this.props.updateTermAndFetchProducts(term, limit, pagination.offset);
  }

  render() {

    const {
      term,
      products
    } = this.props;

    return <div className="products">
      <div className="products__title">
        <p>{term || 'Lista de produtos'}</p>
      </div>
      <div className="products__container">
        <div className="products__counter">
          <p>{products.count} produtos encontrados</p>
        </div>

        <div className="products__list">
          <ProductList products={products}/>
        </div>
        <div className="products__pagination-wrapper">
          <div className="products__quantity-selector">
            <select
              value={this.state.value}
              onChange={this.change.bind(this)}
            >
              <option value="10">10 produtos por página</option>
              <option value="25">25 produtos por página</option>
              <option value="50">50 produtos por página</option>
            </select>
          </div>
          <div className="products__pagination">
            <Pagination />
          </div>
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = (store: any) => ({
  term: store.search.term,
  products: store.search.products,
  pagination: store.pagination
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators({ updateTermAndFetchProducts }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductContainer);

// pagination={{...pagination, total: products.count}}