import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { updateTermAndFetchProducts } from '../../../store/actions'

import './SearchInput.css'

interface Props {
  updateTermAndFetchProducts: Function;
  term: string;
  limit: number;
  offset: number;
}

class SearchInput extends React.Component<Props> {

  render() {
    const {
      updateTermAndFetchProducts,
      term,
      limit,
      offset
    } = this.props;

    return <div className="search">
      <svg className="search__icon" viewBox="0 0 32 32" width="100%" height="100%">
        <title>search</title>
        <path d="M21.6 14.171c0-4.114-3.314-7.429-7.429-7.429s-7.429 3.314-7.429 7.429 3.314 7.429 7.429 7.429 7.429-3.314 7.429-7.429zM22.743 14.171c0 4.8-3.771 8.571-8.571 8.571s-8.571-3.771-8.571-8.571 3.771-8.571 8.571-8.571 8.571 3.771 8.571 8.571z"></path>
        <path d="M18.971 19.886l0.914-0.914 6.286 6.286-0.914 0.914z"></path>
      </svg>
      <input
        onChange={(event) => updateTermAndFetchProducts(event.target.value, limit, offset)}
        value={term}
        className="search__input"
        type="text"
        placeholder="Pesquisar"/>
    </div>
  }
}

const mapStateToProps = (store: any) => ({
  term: store.search.term,
  limit: store.pagination.limit,
  offset: store.pagination.offset
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators({ updateTermAndFetchProducts }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SearchInput);
